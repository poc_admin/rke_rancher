variable "region" {
  type = string
  description = "AWS region where resources are deployed"
}

variable "environment_tag" {
  type = string
  description = "Environment information"
  default = "Administration"
}

variable "admin_ssh_private_key_path" {
  type = string
  description = "SSH private key path allowing access to EC2"
}

variable "rke_nodes" {
  type = list(object({
    hostname = string
    roles = list(string)
  }))
  description = "Map of RKE nodes with "
}