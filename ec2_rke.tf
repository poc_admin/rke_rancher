data "aws_subnet" "rke_rancher" {
  filter {
    name   = "tag:Name"
    values = ["Administration network's RKE subnet"]
  }
}

data "aws_security_group" "admin_rke" {
  name = "admin_rke"
}


resource "aws_instance" "rke" {
  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the NAT Gateway for the VPC subnet.
  for_each = {for node in var.rke_nodes:  node.hostname => node}

  ami                  = "ami-03cc9ed7d3047f81d"
  instance_type        = "t3.medium"
  user_data            = data.template_file.rke_user_data.rendered
  subnet_id            = data.aws_subnet.rke_rancher.id
  iam_instance_profile = aws_iam_instance_profile.rke_profile.name

  root_block_device {
    volume_size = 20
  }
  
  vpc_security_group_ids = [
    data.aws_security_group.admin_rke.id
  ]

  tags = {
    Name = "RKE node"
    Environment = var.environment_tag
  }

  provisioner "remote-exec" {
    connection {
      host = aws_instance.rke[each.key].private_dns
      user = "ansible"
      private_key = file("~/.ssh/id_rsa")
    }

    script = "files/wait-cloud-init.sh"
  }
}

data "template_file" "rke_user_data" {
  template = file("${path.module}/files/ec2_rke.yml")
  vars = {
    ANSIBLE_SSH_KEY = file("${path.module}/files/ansible.pub")
  }
}
