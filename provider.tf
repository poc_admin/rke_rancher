terraform {

  backend "http" {
    address = "https://gitlab.com/api/v4/projects/29495354/terraform/state/tfstate"
    lock_address = "https://gitlab.com/api/v4/projects/29495354/terraform/state/tfstate/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/29495354/terraform/state/tfstate/lock"
    username="jallaix"
    lock_method="POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }

  required_providers {
    rke = {
      source = "rancher/rke"
      version = "1.2.3"
    }
  }
}

provider "aws" {
  region     = var.region
}

provider "rke" {
}