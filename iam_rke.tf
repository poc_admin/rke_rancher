# See: https://rancher.com/blog/2018/2018-05-14-rke-on-aws/

data "aws_iam_policy_document" "rke_assume_role_policy" {

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "rke_role" {
    name               = "rke-role"
    assume_role_policy = data.aws_iam_policy_document.rke_assume_role_policy.json
}

data "aws_iam_policy_document" "rke_access_policy" {

  statement {
    actions = ["ec2:Describe*"]
    effect = "Allow"
    resources = ["*"]
  }
  statement {
    actions = ["ec2:AttachVolume"]
    effect = "Allow"
    resources = ["*"]
  }
  statement {
    actions = ["ec2:DetachVolume"]
    effect = "Allow"
    resources = ["*"]
  }
  statement {
    actions = ["ec2:*"]
    effect = "Allow"
    resources = ["*"]
  }
  statement {
    actions = ["elasticloadbalancing:*"]
    effect = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_role_policy" "rke_access_policy" {
  name = "rke-access-policy"
  role = aws_iam_role.rke_role.id

  policy = data.aws_iam_policy_document.rke_access_policy.json
}

resource "aws_iam_instance_profile" "rke_profile" {
  name = "rke-profile"
  role = aws_iam_role.rke_role.name
}
