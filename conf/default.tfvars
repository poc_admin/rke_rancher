region = "eu-west-3"                     # France (PAris)
admin_ssh_private_key_path = "~/.ssh/ansible.pem"
rke_nodes = [
    { hostname = "rke1", roles = [ "controlplane", "worker", "etcd" ] }
]