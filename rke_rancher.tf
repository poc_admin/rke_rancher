resource "rke_cluster" "rke_rancher" {

  cluster_name = "rancher"

  dynamic "nodes" {
      for_each = {for node in var.rke_nodes:  node.hostname => node}
      content {
        address = aws_instance.rke[nodes.key].private_dns
        user    = "ansible"
        role    = nodes.value.roles
        ssh_key = file("~/.ssh/id_rsa")
      }
  }
}