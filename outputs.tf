output "rke_rancher_kube_config" {
  value = rke_cluster.rke_rancher.kube_config_yaml
  sensitive = true
}